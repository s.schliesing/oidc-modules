terraform {
  required_version = "v1.4.0"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.55.0"
    }
  }
}

# Setting up service OIDC service account
resource "google_service_account" "oidc_sa" {
  account_id   = "oidc-sa"
  display_name = "Service Account for OIDC."
}

# Granting permissions to OIDC service account
# You can change or add roles in for_each section
resource "google_project_iam_member" "oidc_sa_permissions" {
  for_each = toset([
    "roles/secretmanager.secretAccessor",
  ])
  role    = each.value
  member  = google_service_account.oidc_sa.member
  project = var.project_id
}

# Calling OIDC module
module "gl_oidc" {
  source            = "gitlab.com/gitlab-com/gcp-oidc/google"
  version           = "3.0.0"
  google_project_id = var.project_id
  gitlab_project_id = var.gitlab_project_id
  oidc_service_account = {
    "sa" = {
      sa_email  = google_service_account.oidc_sa.email
      attribute = "attribute.project_id/${var.gitlab_project_id}"
    }
  }
}
