# tflint-ignore: terraform_unused_declarations
variable "gitlab_url" {
  type        = string
  description = "URL of GitLab Installation"
  default     = "https://gitlab.com"
}

variable "google_project_id" {
  type        = string
  description = "Project ID of the GCP Project"
}

variable "gitlab_namespace_path" {
  type        = string
  description = "Path of a gitlab namespace (https://docs.gitlab.com/ee/api/namespaces.html)"
  default     = null
}

variable "oidc_service_account" {
  type = map(object({
    sa_email  = string
    attribute = string
  }))
  description = "OIDC service account email and projectID mapping"
}

variable "gitlab_project_id" {
  type        = number
  description = "ID of the GitLab project found in settings"
}

variable "bind_to_namespace" {
  type        = bool
  description = "OIDC condition will be bound to the specified GitLab namespace if this value is set to true. This means that the attribute will check the namespace with `startsWith` and all projects under it will be included in the scope."
  default     = false
}

variable "allowed_audiences" {
  type        = list(string)
  description = "Allowed audience value of tokens(aud claim)"
  default     = []
}

variable "workload_identity_name" {
  type        = string
  description = "Custom Workload Identity Pool name which useful in case of multiple pools in a single project (will be appended after `gitlab-pool-oidc-`)"
  default     = null
}

variable "custom_condition" {
  type        = string
  description = "If this is set, this is the OIDC condition to use to determine of the claim is valid or not"
  default     = null
}
