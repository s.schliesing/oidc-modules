output "workload_identity_pool_provider_name" {
  description = "Name of the Workload Identity Pool Provider. This value is required for the audience attribute in an OIDC exchange."
  value       = google_iam_workload_identity_pool_provider.gitlab_provider_jwt.name
}
