# Create a new Google Cloud Workload Identity Pool
resource "google_iam_workload_identity_pool" "gitlab_pool" {
  workload_identity_pool_id = "gitlab-pool-oidc-${coalesce(var.workload_identity_name, var.gitlab_project_id)}"
  project                   = var.google_project_id
}

# Create a new Google Cloud Workload Identity Provider inside the Workload Identity Pool created in the previous step
# attribute_condition - If bind_to_namespace is set to true, use namespace path otherwise use gitlab project id.
# This way either the provider can be linked with namespace or can be linked to a project itself for more granularity.
resource "google_iam_workload_identity_pool_provider" "gitlab_provider_jwt" {
  workload_identity_pool_id          = google_iam_workload_identity_pool.gitlab_pool.workload_identity_pool_id
  workload_identity_pool_provider_id = "gitlab-jwt-${coalesce(var.workload_identity_name, var.gitlab_project_id)}"
  project                            = var.google_project_id
  attribute_condition                = var.custom_condition != null ? var.custom_condition : (var.bind_to_namespace ? "attribute.namespace_path.startsWith(\"${var.gitlab_namespace_path}\")" : "attribute.project_id == \"${var.gitlab_project_id}\"")
  attribute_mapping = {
    "google.subject"           = "assertion.project_id + \"::\" + assertion.ref", # Required and changed from gitlab.sub to avoid crossing the 127 bytes limit
    "attribute.project_path"   = "assertion.project_path",
    "attribute.project_id"     = "assertion.project_id",
    "attribute.namespace_id"   = "assertion.namespace_id",
    "attribute.namespace_path" = "assertion.namespace_path",
    "attribute.user_email"     = "assertion.user_email",
    "attribute.ref"            = "assertion.ref",
    "attribute.aud"            = "assertion.aud"
    "attribute.ref_type"       = "assertion.ref_type",
    "attribute.sub"            = "assertion.sub",
    "attribute.ci_config_sha"  = "assertion.ci_config_sha",
    "attribute.sha"            = "assertion.sha"
  }
  oidc {
    issuer_uri        = var.gitlab_url
    allowed_audiences = var.allowed_audiences
  }
}

# Grant permissions for Service Account impersonation
resource "google_service_account_iam_binding" "oidc_sa_binding" {
  for_each           = var.oidc_service_account # Loop through each service account and bind it to the principal.
  service_account_id = "projects/${var.google_project_id}/serviceAccounts/${each.value.sa_email}"
  role               = "roles/iam.workloadIdentityUser"
  members = [
    "principalSet://iam.googleapis.com/${google_iam_workload_identity_pool.gitlab_pool.name}/${each.value.attribute}"
  ]
}
