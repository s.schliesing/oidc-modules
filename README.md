# OIDC Modules

[[_TOC_]]

This project provides a guide on how to integrate GitLab with Google Cloud Platform (GCP) using OpenID Connect (OIDC). The integration enables GitLab users to authenticate with their GCP credentials, which helps to improve security and reduce the need for multiple user accounts.

Before starting, you should have the following:

- A GitLab account with administrator access.
- A GCP account with the required permissions to manage service accounts and Workload Identities.
- Basic knowledge of OIDC and its configuration.

## Setup

This section provides a step-by-step guide on how to set up the OIDC integration between GitLab and GCP.

### GCP account permissions to setup OIDC

The user/service account which executes the terraform module should at least have the role of IAM Workload Identity Pool Admin (`roles/iam.workloadIdentityPoolAdmin`).

### Configure GitLab for OIDC Integration using Terraform Module

An example of how to use the [terraform module for OIDC](./terraform-modules/gcp-oidc/) is configured in the [samples folder](./samples/terraform/oidc-gcp/).

```terraform
module "gl_oidc" {
  source = "gitlab.com/gitlab-com/gcp-oidc/google"
  version = "3.0.0"
  google_project_id = GOOGLE_PROJECT_ID
  gitlab_project_id = GITLAB_PROJECT_ID
  oidc_service_account = {
    "sa" = {
      sa_email  = "SERVICE_ACCOUNT_EMAIL"
      attribute = "attribute.project_id/GITLAB_PROJECT_ID"
    }
  }
}
```

Note: You must declare the `google` provider when using this module.

**Parameters**

| Name                   | Description                                                                                                                                                                                                                    | Type                                                                         | Default                | Required |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------- | ---------------------- | :------: |
| allowed_audiences      | Allowed audience value of tokens(aud claim)                                                                                                                                                                                    | `list(string)`                                                               | `[]`                   |    no    |
| bind_to_namespace      | OIDC condition will be bound to the specified GitLab namespace if this value is set to true. This means that the attribute will check the namespace with `startsWith` and all projects under it will be included in the scope. | `bool`                                                                       | `false`                |    no    |
| gitlab_namespace_path  | Path of a gitlab namespace (https://docs.gitlab.com/ee/api/namespaces.html)                                                                                                                                                    | `string`                                                                     | `null`                 |    no    |
| gitlab_project_id      | ID of the GitLab project found in settings                                                                                                                                                                                     | `number`                                                                     | n/a                    |   yes    |
| gitlab_url             | URL of GitLab Installation                                                                                                                                                                                                     | `string`                                                                     | `"https://gitlab.com"` |    no    |
| google_project_id      | Project ID of the GCP Project                                                                                                                                                                                                  | `string`                                                                     | n/a                    |   yes    |
| oidc_service_account   | OIDC service account email and projectID mapping                                                                                                                                                                               | <pre>map(object({<br> sa_email = string<br> attribute = string<br> }))</pre> | n/a                    |   yes    |
| workload_identity_name | Custom Workload Identity Pool name which useful in case of multiple pools in a single project (will be appended after `gitlab-pool-oidc-`)                                                                                     | `string`                                                                     | `null`                 |    no    |

### Test the Integration

Anyone of the [CI samples](samples/ci/gcp/) can be used to test the integration.

## Using OIDC in Pipelines

This section provides instructions on how to use the OIDC integration between GitLab and GCP, including how to log in with GCP credentials and execute some scripts/commands in a CI pipeline.

The CI (Continuous Integration) template in GitLab is a predefined configuration file that helps you automate the process of building, testing, and deploying your software applications. There is a [gcp_auth.yaml](./templates/gcp_auth.yaml) template that has different authentication methods. This template is used in our [CI samples](samples/ci/gcp/) folder.

We provides `.google-oidc:auth:` and `.google-oidc:access_token` template jobs on the template.
The required GitLab version is 16.1.

> Please note that `id_tokens` is being used in the templates which was introduced in [GitLab 15.9](https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#secure-your-cicd-workflows-with-openid-connect-oidc).
> And the latest CI template uses feature which was introduced in [GitLab 16.1](https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules/-/merge_requests/15).

**Required Variables**

- `WI_POOL_PROVIDER` - Full canonical resource name of the workload identity pool provider.
- `SERVICE_ACCOUNT` - Service Account email address

```yaml
  variables:
    WI_POOL_PROVIDER: //iam.googleapis.com/projects/GOOGLE_PROJECT_ID/locations/global/workloadIdentityPools/WORKLOAD_IDENTITY_POOL/providers/WORKLOAD_IDENTITY_POOL_PROVIDER
    SERVICE_ACCOUNT: SERVICE_ACCOUNT_EMAIL
```

**Optional Variables**

- `AUDIENCE` - Default value: The value of `WI_POOL_PROVIDER`. This is a custom configuration of `aud` claim in [id_tokens](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html). We do not recommend changing this value.
- `TOKEN_LIFETIME` - Default value: 1800. The value of lifetime of Google's access token.
- `GCLOUD_PROJECT`(Only for Google CLIs such as `gcloud`, `bq`, or `gsutil`) - Default value: null (Service Account's project is set by default). An initial project ID of Google CLIs.

### Set up Application Default Credentials

`.google-oidc:auth:` - This template job sets the `GOOGLE_APPLICATION_CREDENTIALS` to the credentials file for OIDC auth. This allows us to authenticate and authorize access to Google Cloud resources using our existing third-party account. Most of libraries and tools support this method. and you can use any image you like.

Sample usage can be found [here(Go Application)](./samples/ci/gcp/.go-ci.yml) and [here(gcloud)](./samples/ci/gcp/.gcloud-ci.yml).

### (Optional) Generates an OAuth 2.0 access token for a service account

`.google-oidc:access_token:` - This template job creates an OAuth 2.0 access token for a service account. The access token is set to the `GOOGLE_OAUTH_ACCESS_TOKEN` (used by Terraform) and `CLOUDSDK_AUTH_ACCESS_TOKEN` (used by gcloud) environment variables. This can be helpful in cases where you need to directly handle an access token to make some requests. Note that `curl` and `python3` are required for this method, and you can also create an access token using `gcloud auth print-access-token` via `gcloud cli` method. This can be configured in a similar way to the example above.

### Examples

- Terraform sample usage is located [here](./samples/terraform/oidc-gcp)
- Examples of CI pipelines and different methods are available [here](./samples/ci/gcp/)

## Frequently Asked Questions

### Can we use `before_script` and `after_script` in the code since the provided template uses them?

You can not use `before_script` in the job which uses this template as the way Gitlab CI works will result in OIDC code being overwritten. `after_script` is being used for cleanups only. So, you can overwrite them without any issues.

### How can we authenticate the whole Gitlab namespace rather than a particular project?

This can be done by configuring namespace in the module. Please see the following example on how to do that:

```terraform
module "oidc-configuration" {
  source            = "gitlab.com/gitlab-com/gcp-oidc/google"
  version           = "3.0.0"
  google_project_id = "GCP_PROJECT_ID"
  gitlab_project_id = "GITLAB_PROJECT_ID"
  oidc_service_account = {
    "service_account" = {
      sa_email  = "SERVICE_ACCOUNT)EMAIL"
      attribute = "attribute.aud/CUSTOM_AUDIENCE_VALUE"
    }
  }
  workload_identity_name = "CUSTOM_WI_NAME"
  bind_to_namespace      = true
  gitlab_namespace_path  = "FULL_GITLAB_NAMESPACE_PATH"
  allowed_audiences      = ["CUSTOM_AUDIENCE_VALUE"]
}
```

This will now authenticate the whole namespace rather than just a particular project in it.

## Contributing

If you have any feedback, suggestions, or issues with the project, please feel free to contribute by submitting a pull request or opening an issue in the repository.
