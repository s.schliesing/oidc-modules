# Gitlab-GCP OIDC Module

This module creates OIDC between GCP and GitLab.

Following changes occur when this is executed:

1. Creation of workload Identity Pool
1. Creation of Workload Identity Provider
1. Creation of SA Binding to Workload Identity

## Usage

```
module "gl_oidc" {
  source            = "LOCATION_OF_TERRAFORM_MODULE"
  google_project_id = GOOGLE_PROJECT_ID
  gitlab_project_id = GITLAB_PROJECT_ID
  oidc_service_account = {
    "sa" = {
      sa_email  = "SERVICE_ACCOUNT_EMAIL"
      attribute = "attribute.project_id/GITLAB_PROJECT_ID"
    }
  }
}
```

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= v1.4.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.55 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.55 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_iam_workload_identity_pool.gitlab_pool](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool) | resource |
| [google_iam_workload_identity_pool_provider.gitlab_provider_jwt](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iam_workload_identity_pool_provider) | resource |
| [google_service_account_iam_binding.oidc_sa_binding](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_binding) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_audiences"></a> [allowed\_audiences](#input\_allowed\_audiences) | Allowed audience value of tokens(aud claim) | `list(string)` | `[]` | no |
| <a name="input_bind_to_namespace"></a> [bind\_to\_namespace](#input\_bind\_to\_namespace) | OIDC condition will be bound to the specified GitLab namespace if this value is set to true. This means that the attribute will check the namespace with `startsWith` and all projects under it will be included in the scope. | `bool` | `false` | no |
| <a name="input_custom_condition"></a> [custom\_condition](#input\_custom\_condition) | If this is set, this is the OIDC condition to use to determine of the claim is valid or not | `string` | `null` | no |
| <a name="input_gitlab_namespace_path"></a> [gitlab\_namespace\_path](#input\_gitlab\_namespace\_path) | Path of a gitlab namespace (https://docs.gitlab.com/ee/api/namespaces.html) | `string` | `null` | no |
| <a name="input_gitlab_project_id"></a> [gitlab\_project\_id](#input\_gitlab\_project\_id) | ID of the GitLab project found in settings | `number` | n/a | yes |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | URL of GitLab Installation | `string` | `"https://gitlab.com"` | no |
| <a name="input_google_project_id"></a> [google\_project\_id](#input\_google\_project\_id) | Project ID of the GCP Project | `string` | n/a | yes |
| <a name="input_oidc_service_account"></a> [oidc\_service\_account](#input\_oidc\_service\_account) | OIDC service account email and projectID mapping | <pre>map(object({<br>    sa_email  = string<br>    attribute = string<br>  }))</pre> | n/a | yes |
| <a name="input_workload_identity_name"></a> [workload\_identity\_name](#input\_workload\_identity\_name) | Custom Workload Identity Pool name which useful in case of multiple pools in a single project (will be appended after `gitlab-pool-oidc-`) | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_workload_identity_pool_provider_name"></a> [workload\_identity\_pool\_provider\_name](#output\_workload\_identity\_pool\_provider\_name) | Name of the Workload Identity Pool Provider. This value is required for the audience attribute in an OIDC exchange. |
<!-- END_TF_DOCS -->
